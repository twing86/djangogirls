from django.test import SimpleTestCase
from django.urls import reverse, resolve
from blog.views import post_list, post_detail, post_edit, post_new


class TestUrls(SimpleTestCase):

    def test_url_post_list(self):
        url = reverse('post_list')
        self.assertEqual(resolve(url).func, post_list)

    def test_url_post_detail(self):
        url = reverse('post_detail', args=[15, ])
        self.assertEqual(resolve(url).func, post_detail)

    def test_url_post_edit(self):
        url = reverse('post_edit', args=[1, ])
        self.assertEqual(resolve(url).func, post_edit)

    def test_url_post_new(self):
        url = reverse('post_new')
        self.assertEqual(resolve(url).func, post_new)

